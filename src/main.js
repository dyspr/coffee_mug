var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < 64; i++) {
    fill(255 * ((i + 1) % 2))
    noStroke()
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    rotate(i * (1 / 64) * Math.PI + sin(frameCount * 0.005) * Math.PI)
    ellipse(0, 0, boardSize * (0.75 + 0.1 * cos(frameCount * 0.1 + i * 0.15 + 0 * (i % 2) * Math.PI)) * (1 - (i / 64)), boardSize * (0.75 + 0.1 * sin(frameCount * 0.1 + i * 0.3 + (i % 3) * Math.PI)) * (1 - (i / 64)))
    pop()
  }

}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
